package br.com.joaocabraldev.parcelamentos;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class MainControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void index() {
        assertEquals("main", this.restTemplate.getForObject("http://localhost:8081/", String.class));
    }

    @Test
    void health() {
        assertEquals("OK", this.restTemplate.getForObject("http://localhost:8081/health", String.class));
    }
}