package br.com.joaocabraldev.parcelamentos.cliente;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class ClienteControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void listar() {
        final Cliente cliente1 = Cliente.builder()
                .id(1L)
                .nome("José da Silva")
                .email("josedasilva@gmail.com")
                .telefone("1111-1111")
                .build();

        final Cliente cliente2 = Cliente.builder()
                .id(1L)
                .nome("Maria Ferreira")
                .email("mariaferreira@gmail.com")
                .telefone("2222-2222")
                .build();

        final List<Cliente> expected = Arrays.asList(cliente1, cliente2);

        final ResponseEntity<Cliente[]> response = this.restTemplate.getForEntity("http://localhost:8081/clientes", Cliente[].class);

        final List<Cliente> actual = Arrays.asList(Objects.requireNonNull(response.getBody()));

        assertEquals(expected, actual);
    }
}