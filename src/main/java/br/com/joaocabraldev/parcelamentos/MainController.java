package br.com.joaocabraldev.parcelamentos;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class MainController {

    @GetMapping
    public String index() {
        return "main";
    }

    @GetMapping("/health")
    public String health() {
        return "OK";
    }

}
