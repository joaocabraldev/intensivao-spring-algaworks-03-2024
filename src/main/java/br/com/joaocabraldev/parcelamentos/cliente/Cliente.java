package br.com.joaocabraldev.parcelamentos.cliente;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@Builder
public class Cliente {

    private Long id;

    private String nome;

    private String email;

    private String telefone;

}
