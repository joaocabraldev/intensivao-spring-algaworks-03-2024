package br.com.joaocabraldev.parcelamentos.cliente;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

    @GetMapping
    public List<Cliente> listar() {
        final Cliente cliente1 = Cliente.builder()
                .id(1L)
                .nome("José da Silva")
                .email("josedasilva@gmail.com")
                .telefone("1111-1111")
                .build();

        final Cliente cliente2 = Cliente.builder()
                .id(1L)
                .nome("Maria Ferreira")
                .email("mariaferreira@gmail.com")
                .telefone("2222-2222")
                .build();

        return Arrays.asList(cliente1, cliente2);
    }

}
