package br.com.joaocabraldev.parcelamentos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParcelamentosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParcelamentosApplication.class, args);
	}

}
